import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.movie.getMovie().getTitle() + "\t"
                    + String.valueOf(totalAmount()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount()) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    //Variance method for statement()
    public String htmlStatement() {
        String result = "<h2> Rental record for <b>" + getName()
                + "</b></h2>\n";

        for (Rental rental : rentals) {
            result += rental.movie.getMovie().getTitle()
                    + ":" + String.valueOf(rental.getThisAmount())
                    + "<br>\n";
        }

        result += "<p>Amount owed is " + String.valueOf(totalAmount())
                + "</p>\n";

        result += "<p>You earned " + String.valueOf(frequentRenterPoints())
                + "</p>\n";

        return result;
    }

    //Method created by Replace Temp with Query
    public int frequentRenterPoints() {
        int value = 0;
        for (Rental rental : rentals) {
            value += rental.getFrequentRenterPoints();
        }
        return value;
    }

    //Method created by Replace Temp with Query
    public double totalAmount() {
        double value = 0;
        for (Rental rental : rentals) {
            value += rental.getThisAmount();
        }
        return value;
    }
}