import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {

    private Movie movie, movie1, movie2;
    private Rental rent, rental1, rental2;

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);

        movie1 = new Movie("Gridiron Gang", Movie.REGULAR);
        rental1 = new Rental(movie1, 5);

        movie2 = new Movie("Alice in the Wonderland", Movie.CHILDREN);
        rental2 = new Rental(movie2, 4);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.movie.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void getThisAmoundTest() {
        assertEquals("" + rent.getThisAmount(), "3.5");
        assertEquals("" + rental1.getThisAmount(), "6.5");
        assertEquals("" + rental2.getThisAmount(), "3.0");
    }

    @Test
    public void getFrequentRenterPointsTest() {
        assertEquals(rent.getFrequentRenterPoints(), 1);
        assertEquals(rental1.getFrequentRenterPoints(), 1);
        assertEquals(rental2.getFrequentRenterPoints(), 1);
    }
}