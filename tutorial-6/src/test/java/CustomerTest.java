import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    private Movie movie;
    private Rental rental;
    private Customer customer;

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rental = new Rental(movie, 3);
        customer = new Customer("Alice");
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rental);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        customer.addRental(rental);
        customer.addRental(rental);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("2 frequent renter points"));
    }

    @Test
    public void htmlStatementTest() {
        customer.addRental(rental);
        customer.addRental(rental);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
    }
}