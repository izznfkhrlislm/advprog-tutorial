package tutorial.javari;

import com.sun.rmi.rmid.ExecOptionPermission;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tutorial.javari.animal.Animal;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {
    // TODO Implement me! (additional task)

    @Autowired
    private MockMvc mockMvc;

    private JavariDatabase jd = new JavariDatabase();
    private int sampleID = 5;
    private Animal sampleAnimal = jd.getAnimalById(sampleID);
    private String sampleAnimalJSON = animalToJSON(sampleAnimal);

    public JavariControllerTest() throws IOException, JSONException {
    }

    public String animalToJSON(Animal animal) throws JSONException {
        return new JSONObject().put("id", animal.getId())
                .put("type", animal.getType())
                .put("name", animal.getName())
                .put("gender", animal.getGender())
                .put("length", animal.getLength())
                .put("weight", animal.getWeight())
                .put("condition", animal.getCondition()).toString();
    }

    @Test
    public void getAnimalByIdTest() throws Exception {
        this.mockMvc.perform(get("/javari/3"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"));
    }

    @Test
    public void getByIdNotFoundTest() throws Exception {
        this.mockMvc.perform(get("/javari/100"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value("No animal with id: 100"));
    }

    @Test
    public void getAllAnimalsTest() throws Exception {
        this.mockMvc.perform(get("/javari"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[1].id").value("2"));
    }

    @Test
    public void deleteDataAndAddItBackTest() throws Exception {
        deleteData();
        addData();
    }

    public void deleteData() throws Exception {
        this.mockMvc.perform(delete("/javari/" + sampleID))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].msgType").value("success"))
                .andExpect(jsonPath("$[1].id").value(sampleID));
        this.mockMvc.perform(get("/javari/" + sampleID))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value("No animal with id: " + sampleID));
    }

    public void addData() throws Exception {
        this.mockMvc.perform(post("/javari/").content(sampleAnimalJSON))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].msgType").value("success"))
                .andExpect(jsonPath("$[1].id").value(sampleID));
        this.mockMvc.perform(get("/javari/" + sampleID))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(sampleID));
    }
}