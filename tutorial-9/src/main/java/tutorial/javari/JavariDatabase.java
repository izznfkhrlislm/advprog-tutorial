package tutorial.javari;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.JSONException;
import org.json.JSONObject;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class JavariDatabase {
    static final List<Animal> animalList = new ArrayList<Animal>();

    public Animal getAnimalById(int id) {
        Animal res = null;
        for (Animal animal : animalList) {
            if (id == animal.getId()) {
                res = animal;
            }
        }
        return res;
    }

    public static void readCsv() {
        Scanner scanner;
        try {
            scanner = new Scanner(new File("animal_data.csv"));
            scanner.useDelimiter(",");
            String[] lines;
            while (scanner.hasNextLine()) {
                lines = scanner.nextLine().split(",");
                animalList.add(new Animal(Integer.parseInt(lines[0]), lines[1], lines[2],
                        Gender.parseGender(lines[3]), Double.parseDouble(lines[4]),
                        Double.parseDouble(lines[5]), Condition.parseCondition(lines[6])));
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static String addAnimalToCsv(Animal animal) {
        String[] attributes = {animal.getId().toString(), animal.getType(),
                animal.getName(), animal.getGender().toString(),
                String.valueOf(animal.getLength()),
                String.valueOf(animal.getWeight()),
                animal.getCondition().toString()};
        return String.join(",", attributes);
    }

    private static void addToCsv(String line) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("animal_data.csv"));
            for (Animal animal : animalList) {
                bw.append(addAnimalToCsv(animal)).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Animal deleteAnimalFromCsv(int id) {
        readCsv();
        Animal animalRemoved = null;
        try {
            Scanner scanner = new Scanner(new FileReader("animal_data.csv"));
            String[] lines;
            String line;
            List<String> updatedCsv = new ArrayList<String>();
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                lines = line.split(",");
                if (Integer.parseInt(lines[0]) == id) {
                    animalRemoved = new Animal(Integer.parseInt(lines[0]),
                            lines[1], lines[2], Gender.parseGender(lines[3]),
                            Double.parseDouble(lines[4]),
                            Double.parseDouble(lines[5]),
                            Condition.parseCondition(lines[6]));
                    animalList.remove(animalRemoved);
                } else {
                    updatedCsv.add(line);
                }
            }
            scanner.close();
            BufferedWriter bw = new BufferedWriter(new FileWriter("animal_data.csv"));
            for (String animal : updatedCsv) {
                bw.append(animal).append("\n");
                bw.flush();
            }
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return animalRemoved;
    }

    private static Animal jsonToAnimal(String props) throws JSONException {
        JSONObject createdJson = new JSONObject(props);
        Animal animal = new Animal(createdJson.getInt("id"), createdJson.getString("type"),
                createdJson.getString("name"), Gender.parseGender(createdJson.getString("gender")),
                createdJson.getDouble("length"), createdJson.getDouble("weight"),
                Condition.parseCondition(createdJson.getString("condition")));
        animalList.add(animal);
        return animal;
    }

    public static Animal animalToDatabase(String animalJson) throws JSONException {
        Animal newAnimal = jsonToAnimal(animalJson);
        addAnimalToCsv(newAnimal);
        return newAnimal;
    }
}
