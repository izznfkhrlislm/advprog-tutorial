package tutorial.javari;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static tutorial.javari.JavariDatabase.animalList;
import static tutorial.javari.JavariDatabase.deleteAnimalFromCsv;
import static tutorial.javari.JavariDatabase.readCsv;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import tutorial.javari.animal.Animal;

@RestController
public class JavariController {
    // TODO Implement me!
    private final AtomicLong count = new AtomicLong();

    @RequestMapping(value = "/javari", method = GET)
    @ResponseBody
    public String listOfAnimals() throws JsonProcessingException {
        readCsv();

        if (animalList.size() > 0) {
            ObjectMapper om = new ObjectMapper();
            return om.writerWithDefaultPrettyPrinter().writeValueAsString(animalList);
        } else {
            return "No animal here";
        }
    }

    @RequestMapping(value = "/javari/{id}", method = GET)
    @ResponseBody
    public String findAnimal(@PathVariable int id) throws JsonProcessingException {
        readCsv();
        Optional<Animal> findingAnimal = animalList.stream()
                .filter(target -> target.getId().equals(id)).findFirst();
        if (!findingAnimal.isPresent()) {
            return "No animal with id: " + id;
        } else {
            ObjectMapper om = new ObjectMapper();
            return om.writerWithDefaultPrettyPrinter().writeValueAsString(findingAnimal.get());
        }
    }

    @DeleteMapping(value = "/javari/{id}")
    @ResponseBody
    public String removeAnimal(@PathVariable int id) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        Animal animal = deleteAnimalFromCsv(id);
        if (animal != null) {
            return om.writerWithDefaultPrettyPrinter().writeValueAsString(animal);
        } else {
            return "No animal with id: " + id;
        }
    }

    @PostMapping(value = "/javari/{JSON}")
    @ResponseBody
    public String addAnimal(@PathVariable String json)
            throws JSONException, JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        Animal animal = JavariDatabase.animalToDatabase(json);
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(animal);
    }
}
