package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    public MallardDuck() {
        this.setQuackBehavior(new Quack());
        this.setFlyBehavior(new FlyWithWings());
        this.setSwimBehavior(new Swim());
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
