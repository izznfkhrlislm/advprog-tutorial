package sorting;

import org.junit.Assert;
import org.junit.Test;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here

    @Test
    public void testSlowSort() {
        int[] test = {3,9,6,5,4,2,1,7};
        int[] expectedResult = {1,2,3,4,5,6,7,9};

        test = Sorter.slowSort(test);

        Assert.assertArrayEquals(test, expectedResult);
    }

    @Test
    public void testFastSort() {
        int[] test = {7,8,5};
        int[] expectedResult = {5,7,8};

        test = Sorter.fastSort(test);

        Assert.assertArrayEquals(test, expectedResult);
    }

    @Test
    public void testSlowSearchIsFound() {
        int[] test = {1,2,2,2,2,3,33,3,3,4,5};
        int searchResult = Finder.slowSearch(test, 33);

        Assert.assertEquals(searchResult, 33);
    }

    @Test
    public void testSlowSearchIsNotFound() {
        int[] test = {1,3,4,5};
        int searchResult = Finder.slowSearch(test, 110);

        Assert.assertEquals(searchResult, -1);
    }
}
