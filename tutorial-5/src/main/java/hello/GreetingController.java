package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {
        model.addAttribute("name", name);
        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you are interested to hire me");
        }

        StringBuilder curriculumVitae = new StringBuilder();
        curriculumVitae.append("Name: Izzan Fakhril Islam\n");
        curriculumVitae.append("Birth Date: 1st July, 1998\n");
        curriculumVitae.append("Birth Place: Jakarta\n");
        curriculumVitae.append("Address: Jl. Kemuning VI no. 15 "
                + "RT 05/11 Perumahan Taman Duta Depok\n");
        curriculumVitae.append("Education History: \n");
        curriculumVitae.append("- SD Negeri Pekayon 05 Pagi, East Jakarta\n");
        curriculumVitae.append("- SMP Negeri 91, East Jakarta\n");
        curriculumVitae.append("- SMA Negeri 14, East Jakarta\n");
        curriculumVitae.append("- Universitas Indonesia, Faculty of Computer Science\n");
        model.addAttribute("curriculumVitae", curriculumVitae.toString());

        StringBuilder description = new StringBuilder();
        description.append("I am a Computer Science sophomore at Universitas Indonesia.\n");
        description.append("I have some experiences in projects. Twiggsy Mobile "
                                + "Dev is my first one\n");
        description.append("Also, I like to going to new places and explore the new world.\n");
        model.addAttribute("myDescription", description.toString());

        return "greeting";
    }

}
